import {createAppContainer, createSwitchNavigator} from 'react-navigation';

import AuthNavigator from './auth-navigator';
import AppNavigator from './app-navigator';

const RootNavigator = createSwitchNavigator(
  {
    Auth: AuthNavigator,
    App: AppNavigator,
  },
  {
    initialRouteName: 'Auth',
  },
);
export default createAppContainer(RootNavigator);
// import React from 'react';
// import {View, Text} from 'react-native';
// const HelloWorld = () => (
//   <View>
//     <Text>Hello World</Text>
//   </View>
// );
// export default HelloWorld;
