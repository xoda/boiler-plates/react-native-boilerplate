# React Native Boilerplate

Boiler plate for `react-native` project

## Usage

use this command and change `NewProject` to your _project name_

```
npx react-native init NewProject --template https://gitlab.com/xoda/boiler-plates/react-native-boilerplate.git
```
